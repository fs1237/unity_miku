﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Button39 : MonoBehaviour
{
    public GameObject MainModel;
    public GameObject MainModel_2;
    public GameObject StageModel;
    private Boolean isPausing;
    private string btnName;
	// Use this for initialization
	void Start ()
    {
        StageModel.active = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.touchCount>0&&Input.touches[0].phase==TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;
            if(Physics.Raycast(ray,out hit))
            {
                btnName = hit.transform.name;
                switch (btnName)
                {
                    case "StageController":
                        if(StageModel.active)
                        {
                            StageModel.active = false;
                        }
                        else
                        {
                            StageModel.active = true;
                        }
                        break;
                    case "ScreenShots":
                        System.DateTime now = System.DateTime.Now;
                        string times = now.ToString();
                        times = times.Trim();
                        times = times.Replace("/", "-");
                        string filename = "Screenshot" + times + ".png";
                        //判断是否为Android平台  
                        if (Application.platform == RuntimePlatform.Android)
                        {

                            //截取屏幕  
                            Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
                            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
                            texture.Apply();
                            //转为字节数组  
                            byte[] bytes = texture.EncodeToPNG();

                            string destination = "/sdcard/DCIM/ARphoto";
                            //判断目录是否存在，不存在则会创建目录  
                            if (!Directory.Exists(destination))
                            {
                                Directory.CreateDirectory(destination);
                            }
                            String Path_save = destination + "/" + filename;
                            //存图片  
                            System.IO.File.WriteAllBytes(Path_save, bytes);
                        }
                        break;
                    case "39_Pause":
                        if (!isPausing)
                        {
                            isPausing = true;
                            
                            MainModel.GetComponent<MMD4MecanimModel>().GetAudioSource().Pause();
                            MainModel_2.GetComponent<MMD4MecanimModel>().GetAudioSource().Pause();
                            MainModel.GetComponent<MMD4MecanimModel>().animSyncToAudio = false;
                            MainModel_2.GetComponent<MMD4MecanimModel>().animSyncToAudio = false;
                            MainModel.GetComponent<Animator>().speed = 0.000001F;
                            MainModel_2.GetComponent<Animator>().speed = 0.000001F;
                        }
                        else
                        {                            
                            if (MainModel.GetComponent<MMD4MecanimModel>().GetAudioSource().isPlaying)
                            {
                                MainModel.GetComponent<MMD4MecanimModel>().GetAudioSource().Pause();
                                MainModel_2.GetComponent<MMD4MecanimModel>().GetAudioSource().Pause();
                                MainModel.GetComponent<MMD4MecanimModel>().animSyncToAudio = false;
                                MainModel_2.GetComponent<MMD4MecanimModel>().animSyncToAudio = false;
                                MainModel.GetComponent<Animator>().speed = 0.000001F;
                                MainModel_2.GetComponent<Animator>().speed = 0.000001F;
                                break;
                            }
                            isPausing = false;
                            MainModel.GetComponent<Animator>().speed = 1;
                            MainModel_2.GetComponent<Animator>().speed = 1;
                            MainModel.GetComponent<MMD4MecanimModel>().GetAudioSource().Play();
                            MainModel_2.GetComponent<MMD4MecanimModel>().GetAudioSource().Play();
                            MainModel.GetComponent<MMD4MecanimModel>().animSyncToAudio = true;
                            MainModel_2.GetComponent<MMD4MecanimModel>().animSyncToAudio = true;
                        }
                            break;
                }
            }
        }
		
	}
}
